package grid;

import android.graphics.Color;

import java.util.Random;

public class Tile {
    private int color;

    public Tile(){
        Random rnd = new Random();
        color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    public int getColor(){
        return this.color;
    }

    public void setColor(int color){
        this.color = color;
    }
}
