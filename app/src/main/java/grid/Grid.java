package grid;

import android.util.Log;

public class Grid {

    private Tile[][] tiles;

    public Grid(int rows, int cols){
         tiles = new Tile[rows][cols];
        for(int row = 0; row < getRowCount(); row++) {
            for (int col = 0; col < getColCount(); col++) {
                Tile tile = new Tile();
                tiles[row][col] = tile;
            }
        }
    }


    public Tile getTile(int row, int col){
        return tiles[row][col];
    }

    public int getRowCount(){
        return tiles.length;
    }

    public int getColCount(){
        return tiles[0].length;
    }

}
