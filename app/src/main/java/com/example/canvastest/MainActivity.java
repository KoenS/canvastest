package com.example.canvastest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import grid.Grid;

public class MainActivity extends AppCompatActivity {

    private GameSurface gameSurface;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set fullscreen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);



        Grid grid = new Grid(8,8);
        gameSurface = findViewById(R.id.gameSurface);
        gameSurface.setGrid(grid);


        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Grid grid = new Grid(8,8);
                gameSurface.setGrid(grid);
            }
        });

    }


}
