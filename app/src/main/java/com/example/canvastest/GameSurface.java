package com.example.canvastest;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;

import grid.Grid;
import grid.Tile;

public class GameSurface extends SurfaceView implements SurfaceHolder.Callback  {
    private Grid grid = null;

    private static final int GRIDPADDING = 5;

    public GameSurface(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Log.d("DEBUG", "GameSurface constructor");
        getHolder().addCallback(this);
    }

    public GameSurface(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Log.d("DEBUG", "GameSurface constructor 2");
        getHolder().addCallback(this);
    }


    @Override
    public void draw(Canvas canvas){
        super.draw(canvas);
        Log.d("DEBUG", "Draw");


        //white background.
        canvas.drawColor(Color.WHITE);
        if(grid != null)
            drawGrid(canvas);
    }

    public void setGrid(Grid grid){
        this.grid = grid;
        surfaceChanged(getHolder(),0,getWidth(),getHeight());
    }

    private void drawGrid(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();

        int tileWidth = width/grid.getColCount();
        int tileHeight = height/grid.getRowCount();

        for(int row = 0; row < grid.getRowCount(); row++){
            for(int col = 0; col < grid.getColCount(); col++){
                Tile tile = grid.getTile(row, col);

                Paint tileColor = new Paint();
                tileColor.setColor(tile.getColor());

                canvas.drawRect(tileWidth * col + GRIDPADDING,
                        tileHeight  * row + GRIDPADDING,
                        (tileWidth * col) + tileWidth - GRIDPADDING,
                        (tileHeight  * row) + tileHeight - GRIDPADDING,
                        tileColor);
            }
        }
    }

    private Tile getTile(int x, int y){
        int row = 0;
        int col = 0;
        return grid.getTile(row, col);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        Log.d("DEBUG", "Surface Touched");

        int width = getWidth();
        int height = getHeight();

        int tileWidth = width/grid.getColCount();
        int tileHeight = height/grid.getRowCount();

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            int col = ((int)event.getX() / tileWidth);
            int row = ((int)event.getY() / tileHeight);

            Tile tile = grid.getTile(row, col);
            Random rnd = new Random();
            tile.setColor(Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));

            surfaceChanged(getHolder(),0,getWidth(),getHeight());
        }
        return true;
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d("DEBUG", "Surface created");
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d("DEBUG", "Surface changed");
        Canvas c = getHolder().lockCanvas();
        if(c != null){
            draw(c);
            getHolder().unlockCanvasAndPost(c);

        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d("DEBUG", "Surface destroyed");
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int size;
        if(MeasureSpec.getMode(widthMeasureSpec) == MeasureSpec.EXACTLY ^ MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.EXACTLY) {
            if (MeasureSpec.getMode(widthMeasureSpec) == MeasureSpec.EXACTLY)
                size = width;
            else
                size = height;
        }
        else
            size = Math.min(width, height);
        setMeasuredDimension(size, size);
    }

}
